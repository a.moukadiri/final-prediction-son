import pandas as pd
import numpy as np
import os
import librosa
import spectral
from tqdm import tqdm
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split


le = LabelEncoder()
le.classes_ = np.load('tools/classes.npy')



features = []
class_labels = []

max_pad_len = 174

def extract_features(file_name):

    audio, sample_rate = librosa.load(file_name, res_type='kaiser_fast')
    mfccs = librosa.feature.mfcc(y=audio, sr=sample_rate, n_mfcc=40)
    pad_width = max_pad_len - mfccs.shape[1]
    mfccs = np.pad(mfccs, pad_width=((0, 0), (0, pad_width)), mode='constant')

    return mfccs


def to_categorical(y, num_classes):
    """ 1-hot encodes a tensor """
    return np.eye(num_classes, dtype='float32')[y]


if __name__ == "__main__":
    # Iterate through each sound file and extract the features
    # Set the path to the full UrbanSound dataset
    data_path = '../UrbanSound8K/audio/'
    metadata = pd.read_csv('../UrbanSound8K/metadata/UrbanSound8K.csv')

    for i, row in tqdm(metadata.iterrows()):
        try :

            file_name = os.path.join(os.path.abspath(data_path),'fold'+str(row["fold"])+'/',str(row["slice_file_name"]))
            class_label = row["class"]
            data = extract_features(file_name)
            features.append(data)
            class_labels.append(class_label)
        except: continue


    # Convert features and corresponding classification labels into numpy arrays
    features_mfcc = np.array(features)
    classes = np.array(class_labels)

    classes_encoded = to_categorical(le.transform(classes),10)

    # split the dataset

    x_train, x_test, y_train, y_test = train_test_split(features_mfcc, classes_encoded, test_size=0.25, random_state = 10)


    # save the train, test data
    np.save('tools/x_train_cnn.npy',x_train)
    np.save('tools/y_train_cnn.npy',y_train)
    np.save('tools/x_test_cnn.npy',x_test)
    np.save('tools/y_test_cnn.npy',y_test)
