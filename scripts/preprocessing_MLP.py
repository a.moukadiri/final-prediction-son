import pandas as pd
import numpy as np 
import os
import librosa
import spectral
from tqdm import tqdm
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split 



features = []
class_labels = []
# mfcc = spectral.Spectral(nfilt=40,
#                     ncep=20,
#                     do_dct=True,
#                     lowerf=500,
#                     upperf=5500,
#                     alpha=0.6,
#                     frate=25,
#                     wlen=0.035,
#                     nfft=512,
#                     compression='log',
#                     do_deltas=True,
#                     do_deltasdeltas=False)


# Define function to extract mfcc features
def extract_features(file_name):

    audio, sample_rate = librosa.load(file_name, res_type='kaiser_fast') 
    mfccs = librosa.feature.mfcc(y=audio, sr=sample_rate, n_mfcc=40)
    mfccsscaled = np.mean(mfccs.T,axis=0)
     
    return mfccsscaled


def to_categorical(y, num_classes):
    """ 1-hot encodes a tensor """
    return np.eye(num_classes, dtype='float32')[y]    

if __name__ == "__main__":
    # Set the path to the full UrbanSound dataset 
    data_path = '../UrbanSound8K/audio/'

    metadata = pd.read_csv('../UrbanSound8K/metadata/UrbanSound8K.csv')


    # Iterate through each sound file and extract the features 
    for i, row in tqdm(metadata.iterrows()):

        file_name = os.path.join(os.path.abspath(data_path),'fold'+str(row["fold"])+'/',str(row["slice_file_name"]))
        class_label = row["class"]
        data = extract_features(file_name)
        features.append(data)
        class_labels.append(class_label)    


    # Convert features and corresponding classification labels into numpy arrays
    features_mfcc = np.array(features)
    classes = np.array(class_labels)    

    # Encode the classification labels
    label_encoder = LabelEncoder()
    classes_encoded = to_categorical(label_encoder.fit_transform(classes),10)   
    # split the dataset     

    x_train, x_test, y_train, y_test = train_test_split(features_mfcc, classes_encoded, test_size=0.25, random_state = 10)  

    # save the train, test data and the trained label encoder:  


    np.save('tools/x_train_mlp.npy',x_train)
    np.save('tools/y_train_mlp.npy',y_train)
    np.save('tools/x_test_mlp.npy',x_test)
    np.save('tools/y_test_mlp.npy',y_test)
    np.save('tools/label_encoder.npy',label_encoder)